import Builder from './'

class Page extends Builder
  tag: 'div'
  id: 'page'
  children: ->
    @div['#header'] ->
      @h1.classy 'h', style: { 'background-color': '#22f' }
    @div.wow['#menu'].amaze style: { 'background-color': '#2f2' }
    @ul ->
      @li 'one'
      @li 'two'
      @li 'three'
    @h2 'content title', style: { 'background-color': '#f22' }
    @p '''
    lots of content inside here using
    triple-quote strings
    '''
    @p '''
    the intention is for this to be used
    in place of JSX and interpolated
    strings. Your html will be guaranteed
    valid and it is implemented in a
    CoffeeScript-focused interface
    '''

p = new Page
console.log p.dom.outerHTML