# careml
> Easy DSL to create HTML in CoffeeScript

**CARE Markup Language** is an elegant DOM DSL built using ES6 proxies and CoffeeScript classes. Provides a slightly more OO approach to DOM generation than the likes of [HyperScript](https://github.com/hyperhype/hyperscript) or [JSX](https://facebook.github.io/jsx/).

```coffee
class Page extends Builder then tag: 'div', id: 'page', children: ->
  @div['#header'] ->
    @h1.classy 'h', style: { 'background-color': '#22f' }
  @div.wow['#menu'].amaze style: { 'background-color': '#2f2' }
  @ul ->
    @li 'one'
    @li 'two'
    @li 'three'
  @h2 'content title', style: { 'background-color': '#f22' }
  @p '''
  lots of content inside here using
  triple-quote strings
  '''
  @p '''
  the intention is to use this over JSX or
  interpolated strings. Your html will be
  guaranteed valid since it is generated with
  DOM api and it is implemented with a neat DSL
  that fully utilizes the `@` operator
  '''
```

## Installation

```shell
npm install careml
```
or
```shell
yarn add careml
```

## Usage

```coffee
import Builder from 'careml'

class MyElement extends Builder
  tag: 'section'
  class: ['mine', 'main']
  children: ->
    # Here is where you use the DSL
    @article ->
      @p 'Content here'
      @p 'More content'
```
This class has several shortcuts for making HTML elements but the real heart of it is the DSL inside the `children` function.

## DSL
To build HTML, you provide the tag name as a field of `this` or `@` in CoffeeScript e.g.:
```coffee
@div
```
This creates a `<div></div>` element utilizing the DOM `createElement` method.

You can provide arguments to this just like HyperScript:
```coffee
@h1 'My Amazing Blog'
@h3 'by Yours Truly', style: { 'font-size': '20px' }
```
```html
<h1>My Amazing Blog</h1>
<h3 style="font-size:60px;">by Yours Truly</h3>
```
### Other attributes

Attributes can be added like the `style` element above and they can be interwoven with other children.

```coffee
@a href: '#', 'Click here!'
```

