{ document, Text } = window ? require 'html-element'

isNode = (n) -> n?.nodeName? and n?.nodeType?

TAGS = ['a', 'abbr', 'address', 'area', 'article', 'aside', 'audio', 'b',
  'base', 'bdi', 'bdo', 'blockquote', 'body', 'br', 'button', 'canvas',
  'caption', 'cite', 'code', 'col', 'colgroup', 'data', 'datalist', 'dd', 'del',
  'dfn', 'div', 'dl', 'dt', 'em', 'embed', 'fieldset', 'figcaption', 'figure',
  'footer', 'form', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'head', 'header', 'hr',
  'html', 'i', 'iframe', 'img', 'input', 'ins', 'kbd', 'keygen', 'label',
  'legend', 'li', 'link', 'main', 'map', 'mark', 'meta', 'meter', 'nav',
  'noscript', 'object', 'ol', 'optgroup', 'option', 'output', 'p', 'param',
  'pre', 'progress', 'q', 'rb', 'rp', 'rt', 'rtc', 'ruby', 's', 'samp',
  'script', 'section', 'select', 'small', 'source', 'span', 'strong', 'style',
  'sub', 'sup', 'table', 'tbody', 'td', 'template', 'textarea', 'tfoot', 'th',
  'thead', 'time', 'title', 'tr', 'track', 'u', 'ul', 'var', 'video', 'wbr']

parseObject = (elem, obj) ->
  for key, val of obj
    if typeof val is 'function'
      if /^on\w+/.test key then do (key, val) ->
        elem.addEventListener key[2..], val, no
        elem.destructors.push -> elem.removeEventListener key[2..], val, no
      else
        # XXX: Review observable cruft
        elem[key] = val()
        elem.destructors.push val((newval) -> elem[key] = newval)
    else if key is 'style'
      if typeof val is 'string' then elem.style.cssText = val
      else for prop, sval of val
        if typeof sval is 'function'
          elem.style.setProperty prop, sval()
          elem.destructors.push sval (newval) ->
            elem.style.setProperty prop, newval
        else
          important = sval.match /(.*)\W+!important\W*$/
          if important
            elem.style.setProperty prop, important[1], 'important'
          else
            elem.style.setProperty prop, sval
    else if key is 'attrs'
      elem.setAttribute attr, aval for attr, aval of val
    else if key[0...5] is 'data-'
      elem.setAttribute key, val
    else elem[key] = val
  return elem

class Handler

  get: (target, prop, proxy) ->
    return target[prop] if prop is 'call'
    if prop.charAt(0) is '#'
      target().id = prop[1..]
      return proxy
    else if not target[prop]?
      target().classList.add prop
      return proxy
    else
      return target[prop]

  apply: (target, thisArg, args) ->
    tgt = target() # tricks Proxy into letting me use apply trap on an object
    for arg, i in args
      continue unless arg?
      switch
        when typeof arg is 'string' then tgt.appendChild document.createTextNode arg
        when typeof arg in ['number', 'boolean'] or arg instanceof Date or arg instanceof RegExp
          tgt.appendChild document.createTextNode arg.toString()
        when Array.isArray(arg) then args.splice i + 1, 0, arg...
        when isNode arg then tgt.appendChild arg
        # when arg instanceof Text then tgt.appendChild arg
        when typeof arg is 'object' then parseObject tgt, arg
        when typeof arg is 'function'
          class SubBuilder extends Builder then children: arg
          new SubBuilder tgt
    return tgt

class Builder
  constructor: (tag) ->
    @dom = if isNode tag then tag else document.createElement tag ? @tag ? 'div'
    if @class?
      classes = if typeof @class is 'string' then @class.split(' ')
      else if Array.isArray @class then @class else []
      @dom.classList.add cls for cls in classes
    if @id?
      @dom.setAttribute('id', @id)
    if @attributes?
      @dom.setAttribute(attr, val) for attr, val of @attributes
    
    # Parse and apply any other properties provided in a subclass
    rest = Object.keys (Object.getPrototypeOf this)
      .filter (prop) ->
        not ['tag', 'dom', 'class', 'id', 'atrributes', 'children'].includes prop
      .reduce ((obj, key) => obj[key] = @[key]; obj), {}
    parseObject @dom, rest if Object.keys(rest).length isnt 0

    # A place to hold cleanup functions
    @destructors = []
    
    proxy = new Proxy this,
      get: (target, prop) ->
        return target.create(prop) if prop in TAGS
        return target[prop]
    
    switch
      when typeof @children is 'string'
        @dom.appendChild document.createTextNode @children
      when Array.isArray @children
        throw new TypeError 'children cannot be a static array'
      else @children?.call(proxy)
    
    return proxy

  register: (tag) ->

  create: (tag) ->
    el = document.createElement tag
    @dom.appendChild el
    new Proxy((-> el), new Handler)

export default Builder