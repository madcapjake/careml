import Builder from '../'

test 'simple', ->
  el = new (class extends Builder then tag: 'h1')
  expect el.dom.outerHTML
    .toBe '<h1></h1>'
  el = new (class extends Builder then tag: 'h1', children: 'hello world')
  expect el.dom.outerHTML
    .toBe '<h1>hello world</h1>'

test 'nested', ->
  class Nested extends Builder then tag: 'div', children: ->
    @h1 'Title'
    @p 'Paragraph'
  el = new Nested
  expect el.dom.outerHTML
    .toBe '<div><h1>Title</h1><p>Paragraph</p></div>'

test 'arrays for nesting is okay', ->
  class ArrHTML extends Builder then tag: 'div', children: -> [
    @h1('Title'), @p('Paragraph')
  ]
  el = new ArrHTML
  expect el.dom.outerHTML
    .toBe '<div><h1>Title</h1><p>Paragraph</p></div>'
  
  # Cannot provide a static array due to dynamic construction of children
  makeBadBuilder = ->
    class ErrArrHTML extends Builder then tag: 'div', children: [1, 2]
    new ErrArrHTML
  expect makeBadBuilder
    .toThrow TypeError

test 'can provide an id', ->
  class IdentTest extends Builder then id: 'test'
  el = new IdentTest
  expect el.dom.outerHTML
    .toBe '<div id="test"></div>'

  class IdentChildTest extends Builder then children: ->
    @h1['#title'] 'Amazing'
  el = new IdentChildTest
  expect el.dom.outerHTML
    .toBe '<div><h1 id="title">Amazing</h1></div>'

test 'can provide a class', ->
  class ClassTest extends Builder then class: 'klassy'
  el = new ClassTest
  expect el.dom.outerHTML
    .toBe '<div class="klassy"></div>'

  class ClassSplitTest extends Builder then class: 'a b'
  el = new ClassSplitTest
  expect el.dom.outerHTML
    .toBe '<div class="a b"></div>'

  class ClassArrTest extends Builder then class: ['c', 'd']
  el = new ClassArrTest
  expect el.dom.outerHTML
    .toBe '<div class="c d"></div>'

  class ClassChildTest extends Builder then children: ->
    @h1.e.f 'Wow'
  el = new ClassChildTest
  expect el.dom.outerHTML
    .toBe '<div><h1 class="e f">Wow</h1></div>'

test 'can set properties', ->

  a = new (class extends Builder then tag: 'a', href: 'http://example.com')
  expect a.dom.href
    .toBe 'http://example.com/'
  
  c = new (class extends Builder then tag: 'input', name: 'agree', type: 'checkbox')
  expect c.dom.outerHTML
    .toBe '<input name="agree" type="checkbox">'


test 'sets styles', ->

  div = new (class extends Builder then style: color: 'red')
  expect div.dom.style.color
    .toBe 'red'
  expect div.dom.outerHTML
    .toBe '<div style="color: red;"></div>'
  
  class RedDiv extends Builder then children: ->
    @p style: { color: 'red' }, 'foo'
  expect((new RedDiv).dom.outerHTML)
    .toBe '<div><p style="color: red;">foo</p></div>'
  
test 'sets styles as text', ->

  div = new (class extends Builder then style: 'color: red')
  expect div.dom.style.color
    .toBe 'red'
  expect div.dom.outerHTML
    .toBe '<div style="color: red;"></div>'
  
  class RedDiv extends Builder then children: ->
    @p style: 'color: red', 'foo'
  expect((new RedDiv).dom.outerHTML)
    .toBe '<div><p style="color: red;">foo</p></div>'

test 'sets data attributes', ->

  div = new (class extends Builder then 'data-value': 5)
  expect div.dom.getAttribute 'data-value'
    .toBe '5'
  
  class DivWithData extends Builder then children: ->
    @p 'data-value': 10
  expect (new DivWithData).dom.children[0].getAttribute 'data-value'
    .toBe '10'